const iconMixin = {
  props: {
    dimensions: {
      type: String,
      default: "w-5 h-5",
    },
    color: {
      type: String,
      default: "text-white",
    },
  },
  computed: {
    classList: function () {
      return ["fill-current", this.dimensions, this.color];
    },
  },
};

export default iconMixin;
