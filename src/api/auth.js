import {
  hashUserEmailEndpoint,
  getUserInformationEndpoint,
} from "./_endpoints";

import { apiSettings } from "./_settings";

const hashUserEmail = async (email) => {
  const payload = {
    email: email,
  };

  return fetch(hashUserEmailEndpoint, {
    method: "POST",
    body: JSON.stringify(payload),
    ...apiSettings.fetchSettings,
  }).then((response) => {
    if (response.status === 200) {
      return response.json().then((data) => {
        return {
          status: "Success",
          payload: data,
        };
      });
    } else {
      return response.json().then((data) => {
        return {
          status: "Error",
          message: data.msg,
        };
      });
    }
  });
};

const getUserInformation = async (hashedEmail) => {
  const payload = {
    hashed_email: hashedEmail,
  };

  return fetch(getUserInformationEndpoint, {
    method: "POST",
    body: JSON.stringify(payload),
    ...apiSettings.fetchSettings,
  }).then((response) => {
    if (response.status === 200) {
      return response.json().then((data) => {
        return {
          status: "Success",
          payload: data,
        };
      });
    } else {
      return response.json().then((data) => {
        return {
          status: "Error",
          message: data.msg,
        };
      });
    }
  });
};

export { hashUserEmail, getUserInformation };
