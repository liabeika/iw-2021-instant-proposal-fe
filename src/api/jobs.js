import {
  getNextJobEndpoint,
  approveJobEndpoint,
  rejectJobEndpoint,
  getMyApprovedJobsEndpoint,
  applyOnJobFromListEndpoint,
  rejectJobFromListEndpoint,
} from "./_endpoints";

import { apiSettings } from "./_settings";

const getNextJob = async (id) => {
  const payload = {
    user_id: id,
  };

  return fetch(getNextJobEndpoint, {
    method: "POST",
    body: JSON.stringify(payload),
    ...apiSettings.fetchSettings,
  }).then((response) => {
    if (response.status === 200) {
      return response.json().then((data) => {
        return {
          status: "Success",
          payload: data,
        };
      });
    } else {
      return response.json().then((data) => {
        return {
          status: "Error",
          message: data.msg,
        };
      });
    }
  });
};

const approveJob = async (userId, docID) => {
  const payload = {
    user_id: userId,
    doc_id: docID,
  };

  return fetch(approveJobEndpoint, {
    method: "POST",
    body: JSON.stringify(payload),
    ...apiSettings.fetchSettings,
  }).then((response) => {
    if (response.status === 200) {
      return response.json().then((data) => {
        return {
          status: "Success",
          payload: data,
        };
      });
    } else {
      return response.json().then((data) => {
        return {
          status: "Error",
          message: data.msg,
        };
      });
    }
  });
};

const rejectJob = async (userId, docID) => {
  const payload = {
    user_id: userId,
    doc_id: docID,
  };

  return fetch(rejectJobEndpoint, {
    method: "POST",
    body: JSON.stringify(payload),
    ...apiSettings.fetchSettings,
  }).then((response) => {
    if (response.status === 200) {
      return response.json().then((data) => {
        return {
          status: "Success",
          payload: data,
        };
      });
    } else {
      return response.json().then((data) => {
        return {
          status: "Error",
          message: data.msg,
        };
      });
    }
  });
};

const getMyApprovedJobs = async (userId) => {
  const payload = {
    user_id: userId,
  };

  return fetch(getMyApprovedJobsEndpoint, {
    method: "POST",
    body: JSON.stringify(payload),
    ...apiSettings.fetchSettings,
  }).then((response) => {
    if (response.status === 200) {
      return response.json().then((data) => {
        return {
          status: "Success",
          payload: data,
        };
      });
    } else {
      return response.json().then((data) => {
        return {
          status: "Error",
          message: data.msg,
        };
      });
    }
  });
};

const applyOnJobFromList = async (userId, docId) => {
  const payload = {
    user_id: userId,
    doc_id: docId,
  };

  return fetch(applyOnJobFromListEndpoint, {
    method: "POST",
    body: JSON.stringify(payload),
    ...apiSettings.fetchSettings,
  }).then((response) => {
    if (response.status === 200) {
      return response.json().then((data) => {
        return {
          status: "Success",
          payload: data,
        };
      });
    } else {
      return response.json().then((data) => {
        return {
          status: "Error",
          message: data.msg,
        };
      });
    }
  });
};

const rejectJobFromList = async (userId, docId) => {
  const payload = {
    user_id: userId,
    doc_id: docId,
  };

  return fetch(rejectJobFromListEndpoint, {
    method: "POST",
    body: JSON.stringify(payload),
    ...apiSettings.fetchSettings,
  }).then((response) => {
    if (response.status === 200) {
      return response.json().then((data) => {
        return {
          status: "Success",
          payload: data,
        };
      });
    } else {
      return response.json().then((data) => {
        return {
          status: "Error",
          message: data.msg,
        };
      });
    }
  });
};

export {
  getNextJob,
  approveJob,
  rejectJob,
  getMyApprovedJobs,
  applyOnJobFromList,
  rejectJobFromList,
};
