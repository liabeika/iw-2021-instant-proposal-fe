const backEndHostname = () => {
  return "https://48283187b80e61.localhost.run/";
  // return `http://localhost:5550`;
  // return `https://ip-be.exista.dev`;
};

const hashUserEmailEndpoint = `${backEndHostname()}/hash-user-email`;
const getUserInformationEndpoint = `${backEndHostname()}/get-user-information`;
const getNextJobEndpoint = `${backEndHostname()}/get-next-job`;
const approveJobEndpoint = `${backEndHostname()}/approve-job`;
const rejectJobEndpoint = `${backEndHostname()}/reject-job`;
const getMyApprovedJobsEndpoint = `${backEndHostname()}/get-approved-jobs`;
const applyOnJobFromListEndpoint = `${backEndHostname()}/apply-on-job-from-list`;
const rejectJobFromListEndpoint = `${backEndHostname()}/reject-job-from-list`;

export {
  hashUserEmailEndpoint,
  getUserInformationEndpoint,
  getNextJobEndpoint,
  approveJobEndpoint,
  rejectJobEndpoint,
  getMyApprovedJobsEndpoint,
  applyOnJobFromListEndpoint,
  rejectJobFromListEndpoint,
};
