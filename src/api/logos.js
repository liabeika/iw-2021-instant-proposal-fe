const getDomains = (query) => {
  return fetch(
    `https://autocomplete.clearbit.com/v1/companies/suggest?query=${query}`
  ).then((res) => res.json());
};

export { getDomains };
