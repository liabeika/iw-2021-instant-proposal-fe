const apiSettings = {
  get fetchSettings() {
    return {
      mode: "cors",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json",
      },
      redirect: "follow",
      referrerPolicy: "no-referrer",
    };
  },
};

export { apiSettings };
