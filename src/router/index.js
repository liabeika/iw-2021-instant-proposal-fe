import { createRouter, createWebHistory } from "vue-router";

import SignIn from "../views/SignIn.vue";
import JobOffer from "../views/JobOffer.vue";
import MyProfile from "../views/MyProfile.vue";
import MyJobs from "../views/MyJobs.vue";

const routes = [
  {
    path: "/",
    redirect: "/sign-in",
  },
  {
    path: "/sign-in",
    component: SignIn,
  },
  {
    path: "/job-offer",
    component: JobOffer,
  },
  {
    path: "/my-profile",
    component: MyProfile,
  },
  {
    path: "/my-jobs",
    component: MyJobs,
  },
];

let router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
