import { createStore } from "vuex";

import SignIn from "./modules/SignIn";
import Jobs from "./modules/Jobs";

const store = createStore({
  modules: {
    SignIn,
    Jobs,
  },
});

export default store;
