const SignIn = {
  state: {
    userEmail: "",
    userId: "",
    userInformation: {},
  },
  getters: {
    getUserEmail: (state) => {
      return state.userEmail;
    },
    getUserId: (state) => {
      return state.userId;
    },
    getUserInformation: (state) => {
      return state.userInformation;
    },
  },
  mutations: {
    setUserEmail(state, value) {
      state.userEmail = value;
    },
    setUserId(state, value) {
      state.userId = value;
    },
    setUserInformation(state, payload) {
      state.userInformation = {
        ...payload,
      };
    },
  },
  actions: {
    SET_USER_EMAIL(context, payload) {
      context.commit("setUserEmail", payload);
    },
    SET_USER_ID(context, payload) {
      context.commit("setUserId", payload);
    },
    SET_USER_INFORMATION(context, payload) {
      context.commit("setUserInformation", payload);
    },
  },
};

export default SignIn;
