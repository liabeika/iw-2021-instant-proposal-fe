const Jobs = {
  state: {
    jobToShow: {},
    myApprovedJobs: [],
  },
  getters: {
    getJobToShow: (state) => {
      return state.jobToShow;
    },
    getMyApprovedJobs: (state) => {
      return state.myApprovedJobs;
    },
  },
  mutations: {
    setJobToShow(state, payload) {
      state.jobToShow = {
        ...payload,
      };
    },
    setMyApprovedJobs(state, payload) {
      state.myApprovedJobs = [...payload];
    },
  },
  actions: {
    SET_JOB_TO_SHOW(context, payload) {
      context.commit("setJobToShow", payload);
    },
    SET_MY_APPROVED_JOBS(context, payload) {
      context.commit("setMyApprovedJobs", payload);
    },
  },
};

export default Jobs;
