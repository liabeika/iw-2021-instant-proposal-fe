import { createApp } from "vue";
import App from "./App.vue";

import router from "./router";
import store from "./store";

import { createMetaManager } from "vue-meta";
import Vue3TouchEvents from "vue3-touch-events";

import "./index.css";

const metaManager = createMetaManager();

const app = createApp(App)
  .use(router)
  .use(store)
  .use(metaManager)
  .use(Vue3TouchEvents);
app.mount("#app");
